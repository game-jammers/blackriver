//
// Game Jammers 2017
//

using blacktriangles;
using UnityEngine;
using UnityEngine.UI;

namespace GameJammers.BlackRiver
{
    public class InventoryItemPanel
        : UIListElement
    {
        // members /////////////////////////////////////////////////////////////
        public InventoryItem item                           { get { return data as InventoryItem; } }
        public Image image                                  = null;

        // public methods //////////////////////////////////////////////////////
        public override void Refresh()
        {
            base.Refresh();
            if( item != null )
            {
                image.sprite = item.icon;
            }
        }

        public void Use()
        {
            GameSceneController control = SceneController.GetInstance<GameSceneController>();
            control.UseInventoryItem( item.type );
        }

        public void Drop()
        {
            GameManager.instance.inventory.RemoveItem( item );
        }
    }
}
