//
// Game Jammers 2017
//

using blacktriangles;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    public class InventoryPanel
        : UIElement
    {
        // members /////////////////////////////////////////////////////////////
        public UIList itemList                              = null;

        // public methods //////////////////////////////////////////////////////
        public override void Refresh()
        {
            base.Refresh();
            itemList.SetData( GameManager.instance.inventory.items );
        }
    }
}
