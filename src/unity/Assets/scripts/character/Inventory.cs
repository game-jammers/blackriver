//
// Game Jammers 2017
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    [System.Serializable]
    public class Inventory
    {
        // constants ///////////////////////////////////////////////////////////

        // members /////////////////////////////////////////////////////////////
        public InventoryItem[] items                        { get { return _items.ToArray(); } }
        [SerializeField] List<InventoryItem> _items         = new List<InventoryItem>();
        [SerializeField] int inventorySize                  = 4;

        // public methods //////////////////////////////////////////////////////
        public bool AddItem( InventoryItem item )
        {
            foreach( InventoryItem old in _items )
            {
                if( old.type == item.type ) return true;
            }

            if( _items.Count < inventorySize )
            {
                _items.Add( item );
                GameSceneController sceneController = SceneController.GetInstance<GameSceneController>();
                if( sceneController != null )
                {
                    sceneController.inventoryPanel.Refresh();
                }
                return true;
            }

            return false;
        }

        public void RemoveItem( InventoryItem item )
        {
            if( _items.Remove( item ) )
            {
                GameSceneController sceneController = SceneController.GetInstance<GameSceneController>();
                if( sceneController != null )
                {
                    sceneController.inventoryPanel.Refresh();
                }
            }
        }
    }
}
