//
// Game Jammers 2017
//

using blacktriangles;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    public class Character
        : MonoBehaviour
    {
        // types ///////////////////////////////////////////////////////////////
        public enum ClickType
        {
            Left = 0,
            Right = 1,
            Middle = 2
        }

        // members /////////////////////////////////////////////////////////////
        public GameSceneController sceneController          { get { return GetSceneController(); } }
        public Animator animator                            = null;
        public SpriteRenderer sprite                        { get { return GetSpriteRenderer(); } }
        public float moveSpeed                              = 1.0f;

        public Vector3[] path                               = null;
        public bool hasPath                                 { get { return path != null && path.Length > 0;  } }
        private int pathIndex                                = -1;

        public Interactable target                          { get; private set; }
        private ClickType clickType                         = ClickType.Left;

        private GameSceneController _sceneController        = null;
        private SpriteRenderer _sprite                      = null;

        public bool AllowedToMove                           = true;

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Start()
        {
            UpdateVisuals();
        }

        protected virtual void Update()
        {
            UpdateInput();

            if( hasPath && path.Length > 0 && pathIndex >= 0 )
            {
                if( pathIndex >= path.Length )
                {
                    path = null;
                    if( target != null )
                    {
                        DoInteract( target, clickType );
                    }
                }
                else
                {
                    for( int i = 0; i < path.Length - 1; ++i )
                    {
                        Debug.DrawLine( path[i], path[i+1], Color.green, 1.0f );
                    }

                    Vector3 target = path[ pathIndex ];
                    Vector3 diff = target - transform.position;
                    if( diff.sqrMagnitude <= 0.01f )
                    {
                        ++pathIndex;
                    }
                    else
                    {
                        Move( diff.normalized * moveSpeed * Time.deltaTime );
                    }
                }
            }

            animator.SetBool( "moving", hasPath );
        }

        // private methods /////////////////////////////////////////////////////
        private void UpdateInput()
        {
            Interactable mousePicked = null;

            bool leftClickDown = Input.GetMouseButtonDown( (int)ClickType.Left );
            bool rightClickDown = Input.GetMouseButtonDown( (int)ClickType.Right );
            bool leftClickUp = Input.GetMouseButtonUp( (int)ClickType.Left );
            bool rightClickUp = Input.GetMouseButtonUp( (int)ClickType.Right );

            if( leftClickUp || rightClickUp || leftClickDown || rightClickDown )
            {
                mousePicked = SceneController.instance.sceneCam.MousePick2D<Interactable>( Input.mousePosition );
                if( mousePicked != null )
                {
                    if( leftClickDown )
                    {
                        target = mousePicked;
                        clickType = ClickType.Left;
                    }
                    else if( rightClickDown )
                    {
                        target = mousePicked;
                        clickType = ClickType.Right;
                    }
                    else if( (leftClickUp && target == mousePicked && clickType == ClickType.Left)
                        || ( rightClickUp && target == mousePicked && clickType == ClickType.Right) )
                    {
                        TryInteract( mousePicked, clickType );
                    }
                }
                else if( leftClickUp && AllowedToMove )
                {
                    // raycast for interactable
                    Vector3 cursorWorldPos = sceneController.sceneCam.ScreenToWorld( Input.mousePosition, Vector3.forward );

                    Performance.Profile( "Pathing", ()=>{
                            StartPathing( cursorWorldPos );
                        });
                }
            }
        }

        private void UpdateVisuals()
        {
            float overage = transform.position.y - 1.5f;
            float shrinkage = Mathf.Clamp( overage / 1.5f, 0.0f, 1.0f );
            sprite.transform.localScale = Vector3.one - ( Vector3.one * 0.15f * shrinkage );

            if( transform.position.y > 2.5f )
            {
                sprite.sortingOrder = -10;
            }
            else if( transform.position.y < 1.4f )
            {
                sprite.sortingOrder = 10;
            }
            else
            {
                sprite.sortingOrder = 0;
            }
        }

        private void TryInteract( Interactable interact, ClickType clickType )
        {
            if( interact.isCharacterInReach() )
            {
                DoInteract( interact, clickType );
            }
            else
            {
                NavigationMesh.NavNode closestNode = sceneController.navigationMesh.GetClosestNavNode( interact.transform.position );
                if( closestNode != null )
                {
                    StartPathing( closestNode.worldPos );
                }
                else
                {
                    Debug.Log( "Couldn't find a close node?" );
                }
            }
        }

        private void DoInteract( Interactable interact, ClickType clickType )
        {
            interact.ActionPressed( (int)clickType );
            target = null;
        }

        private void Move( Vector3 delta )
        {
            if( delta.x < 0.0f )
            {
                sprite.flipX = true;
            }
            else if( delta.x > 0.0f )
            {
                sprite.flipX = false;
            }

            transform.position += delta;

            UpdateVisuals();
        }

        private void StartPathing( Vector3 destination )
        {
            sceneController.navigationMesh.GetPath( transform.position, destination, ( _path )=>{
                path = _path;
                pathIndex = 0;
            });
        }

        private GameSceneController GetSceneController()
        {
            if( _sceneController == null )
            {
                _sceneController = SceneController.GetInstance<GameSceneController>();
            }

            return _sceneController;
        }

        private SpriteRenderer GetSpriteRenderer()
        {
            if( _sprite == null )
            {
                _sprite = animator.GetComponent<SpriteRenderer>();
            }

            DebugUtility.Assert( _sprite != null, "SpriteRenderer must be attached to Animator" );
            return _sprite;
        }
    }
}
