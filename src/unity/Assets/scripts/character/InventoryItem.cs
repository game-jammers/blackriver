//
// Game Jammers 2017
//

using blacktriangles;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    [System.Serializable]
    public class InventoryItem
    {
        // types ///////////////////////////////////////////////////////////////
        public enum Type
        {
            RadioStatic,
            ExampleItem,
            WetClothes,
            ParrotCage,
            AKnife,
            ABlender,
            SomeKeys,
            SmallTV,
            WindChimes,
            WoodBlock,
        };

        // members /////////////////////////////////////////////////////////////
        public Type type;
        public Sprite icon;
        public string displayName;
        [Multiline] public string description;
        public AudioClip sound;
    }
}
