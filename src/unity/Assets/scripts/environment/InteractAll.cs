//
// Game Jammers 2017
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    public class InteractAll
        : Interactable
    {
        // constants ///////////////////////////////////////////////////////////

        // members /////////////////////////////////////////////////////////////
        public List<Interactable> targets                    = new List<Interactable>();

        // public methods //////////////////////////////////////////////////////
        public override void Interact()
        {
            foreach( Interactable target in targets )
            {
                target.Interact();
            }
        }

        public override void Listen()
        {
            foreach( Interactable target in targets )
            {
                target.Listen();
            }
        }
    }
}
