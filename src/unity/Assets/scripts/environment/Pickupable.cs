//
// Game Jammers 2017
//

using blacktriangles;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    [RequireComponent(typeof(AudioSource))]
    public class Pickupable
        : Interactable
    {
        // constants ///////////////////////////////////////////////////////////

        // members /////////////////////////////////////////////////////////////
        public InventoryItem item                           = default(InventoryItem);
        public AudioSource audioSource                      { get; private set; }
        public Interactable nextSuccess                     = null;
        public Interactable nextFail                        = null;

        // unity callbacks /////////////////////////////////////////////////////
        public override void Awake()
        {
            base.Awake();
            audioSource = GetComponent<AudioSource>();
        }

        // public methods //////////////////////////////////////////////////////
        public override void Interact()
        {
            if( GameManager.instance.inventory.AddItem( item ) )
            {
                audioSource.clip = item.sound;
                audioSource.Play();
                if( nextSuccess != null )
                {
                    nextSuccess.Interact();
                }
            }
            else if( nextFail != null )
            {
                nextFail.Interact();
            }
        }

        public override void Listen()
        {
            Interact();
        }
    }
}
