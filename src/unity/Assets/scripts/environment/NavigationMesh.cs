//
// Game Jammers, 2017
//

using blacktriangles;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    public class NavigationMesh
        : MonoBehaviour
    {
        // constants ///////////////////////////////////////////////////////////
        public const float kStep                              = 0.5f;

        // types ///////////////////////////////////////////////////////////////
        public class NavNode
        {
            public int id                                   = 0;
            public bool walkable                            = true;
            public Vector3 worldPos                         = Vector3.zero;
            public IntVec2 gridPos                          = IntVec2.zero;
            public List<NavNode> connections                = new List<NavNode>();

            public Vector3[] gridVerts
            {
                get
                {
                    Vector3[] result = new Vector3[ 4 ];
                    const float halfStep = kStep * 0.5f;
                    Vector2 min = new Vector2( worldPos.x - halfStep, worldPos.y - halfStep );
                    Vector2 max = new Vector2( worldPos.x + halfStep, worldPos.y + halfStep );
                    result[0] = new Vector3( min.x, min.y, 0f );
                    result[1] = new Vector3( max.x, min.y, 0f );
                    result[2] = new Vector3( max.x, max.y, 0f );
                    result[3] = new Vector3( min.x, max.y, 0f );
                    return result;
                }
            }

            public float fscore                             = Mathf.Infinity;
            public float gscore                             = Mathf.Infinity;
        };

        // members /////////////////////////////////////////////////////////////
        public IntVec2 dims                                 = IntVec2.zero;
        public NavNode[] navNodes                           { get; private set; }

        [SerializeField] List<Rect> areas                   = new List<Rect>();

        // public methods //////////////////////////////////////////////////////
        public void GetPath( Vector3 startPos, Vector3 endPos, System.Action<Vector3[]> callback )
        {
            GetPathCoroutine( startPos, endPos, callback);
        }

        private void GetPathCoroutine( Vector3 startPos, Vector3 endPos, System.Action<Vector3[]> callback )
        {
            NavNode start = GetNode( startPos );

            if( start == null )
            {
                callback( null );
                return;
            }

            NavNode end = GetNode( endPos );
            if( end == null )
            {
                callback( null );
                return;
            }

            foreach( NavNode node in navNodes )
            {
                if( node != null )
                {
                    node.fscore = Mathf.Infinity;
                    node.gscore = Mathf.Infinity;
                }
            }

            List<NavNode> visited = new List<NavNode>();
            Deque<NavNode> stack = new Deque<NavNode>();
            stack.AddToFront( start );
            Dictionary< int, NavNode > cameFrom = new Dictionary< int, NavNode >();

            start.gscore = 0;
            start.fscore = ( start.worldPos - end.worldPos ).magnitude;

            Deque<NavNode> finalNavNodePath = new Deque<NavNode>();
            bool foundPath = false;

            int depth = 0;
            while( stack.Count > 0 && foundPath == false )
            {
                NavNode current = stack.GetFront();
                foreach( NavNode node in stack )
                {
                    if( node.fscore < current.fscore )
                    {
                        current = node;
                    }
                }

                if( current == end )
                {
                    finalNavNodePath.AddToFront( current );
                    while( cameFrom.ContainsKey( current.id ) )
                    {
                        current = cameFrom[ current.id ];
                        finalNavNodePath.AddToFront( current );
                    }
                    foundPath = true;
                }
                else
                {
                    ++depth;
                    stack.Remove( current );
                    visited.Add( current );
                    foreach( NavNode neighbor in current.connections )
                    {
                        if( neighbor == null )
                            continue;

                        if( visited.Contains( neighbor ) )
                            continue;

                        float tentative_gscore = current.gscore + ( current.worldPos - neighbor.worldPos ).magnitude;
                        if( stack.Contains( neighbor ) == false )
                        {
                            stack.AddToFront( neighbor );
                        }
                        else if( tentative_gscore > neighbor.gscore )
                        {
                            continue;
                        }
                        else if( depth > 10 )
                        {
                            continue;
                        }

                        cameFrom[ neighbor.id ] = current;
                        neighbor.gscore = tentative_gscore;
                        neighbor.fscore = neighbor.gscore + depth + ( neighbor.worldPos - end.worldPos ).magnitude;
                    }
                }
            }

            if( foundPath == false )
            {
                callback( null );
                return;
            }

            List<Vector3> result = new List<Vector3>();
            foreach( NavNode next in finalNavNodePath )
            {
                result.Add( next.worldPos );
            }

            callback( result.ToArray() );
        }

        public void RebuildGraph( Camera cam )
        {
            dims = new IntVec2( 30, 15 );
            navNodes = new NavNode[ dims.x * dims.y ];

            for( int y = 0; y < dims.y; ++y )
            {
                for( int x = 0; x < dims.x; ++x )
                {
                    NavNode node = new NavNode();
                    node.gridPos = new IntVec2( x, y );
                    node.id = GetIdFromGridPos( node.gridPos );
                    node.worldPos = new Vector3( x * kStep, y * kStep, 0.0f );

                    if( CheckWalkable( node.worldPos ) )
                    {
                        navNodes[node.id] = node;
                    }
                    else
                    {
                        navNodes[node.id] = null;
                    }
                }
            }

            for( int y = 0; y < dims.y; ++y )
            {
                for( int x = 0; x < dims.x; ++x )
                {
                    NavNode self = GetNavNode( x, y );
                    if( self == null ) continue;

                    for( int j = -1; j < 2; ++j )
                    {
                        for( int k = -1; k < 2; ++k )
                        {
                            if( j == 0 && k == 0 ) continue;
                            int xx = x + k;
                            int yy = y + j;
                            NavNode neighbor = GetNavNode( xx, yy );
                            if( neighbor != null )
                            {
                                self.connections.Add(neighbor);
                            }
                        }
                    }
                }
            }
        }

        public NavNode GetClosestNavNode( Vector3 targetPos )
        {
            const int kMaxSearchDepth = 250;

            IntVec2 gridPos = GetGridPosFromWorld( targetPos );
            int depth = 0;
            bool found = false;
            Deque<IntVec2> stack = new Deque<IntVec2>();
            List<IntVec2> visited = new List<IntVec2>();
            stack.AddToFront( gridPos );
            while( !found && depth < kMaxSearchDepth )
            {
                IntVec2 check = stack.RemoveFromFront();
                visited.Add( check );
                NavNode node = GetNavNode( check );
                if( node != null )
                {
                    return node;
                }
                else
                {
                    for( int y = -1; y < 2; ++y )
                    {
                        for( int x = -1; x < 2; ++x )
                        {
                            IntVec2 next = check + new IntVec2(x,y);
                            if( visited.Contains( next ) == false )
                            {
                                stack.AddToBack( next );
                            }
                        }
                    }
                }
                ++depth;
            }

            return null;
        }

        // private methods /////////////////////////////////////////////////////
        public NavNode GetNode( Vector3 worldPos )
        {
            IntVec2 gridPos = GetGridPosFromWorld( worldPos );
            return GetNavNode( gridPos );
        }

        public NavNode GetNavNode( int x, int y )
        {
            return GetNavNode( new IntVec2(x,y) );
        }

        public NavNode GetNavNode( IntVec2 gridPos )
        {
            return GetNavNode( GetIdFromGridPos( gridPos ) );
        }

        public NavNode GetNavNode( int id )
        {
            if( navNodes.IsValidIndex(id) )
            {
                return navNodes[id];
            }
            return null;
        }

        public bool CheckWalkable( Vector3 pos )
        {
            foreach( Rect area in areas )
            {
                if( area.Contains( pos.ToVector2XY() ) )
                {
                    return true;
                }
            }

            return false;
        }

        public IntVec2 GetGridPosFromWorld( Vector3 worldPos )
        {
            return new IntVec2( (int)Mathf.Round(worldPos.x / kStep), (int)Mathf.Round(worldPos.y / kStep) );
        }

        public IntVec2 GetGridPosFromId( int id )
        {
            return new IntVec2( id % dims.x, id / dims.x );
        }

        public int GetIdFromGridPos( IntVec2 pos )
        {
            return pos.y * dims.x + pos.x;
        }

        // unity callbacks /////////////////////////////////////////////////////
        protected virtual void Awake()
        {
            RebuildGraph( SceneController.instance.sceneCam.unityCamera );
        }
    }
}
