//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

namespace GameJammers.BlackRiver
{
  public class SceneManager
    : BaseSceneManager
  {
    // constants //////////////////////////////////////////////////////////////
    public static readonly string kPrefabPath               = "managers/SceneManager";

    // accessors //////////////////////////////////////////////////////////////
    public static new SceneManager instance                 { get; private set; }

    // public methods /////////////////////////////////////////////////////////
    public static SceneManager EnsureExists()
    {
      if( instance == null )
      {
        instance = EnsureExists<SceneManager>( kPrefabPath );
        instance.name = "SceneManager";
      }

      return instance;
    }
  };
}
