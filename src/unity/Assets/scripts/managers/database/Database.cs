//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;
using System.Collections.Generic;

namespace GameJammers.BlackRiver
{
	public static class Database
	{
	    // members /////////////////////////////////////////////////////////////
        public static StringTable stringtable               = null;

        public static bool initialized                      { get; private set; }

	    // constructor / destructor ////////////////////////////////////////////
	    static Database()
	    {
            Initialize( true );
	    }

		public static void Initialize( bool force = false )
		{
            if( force || initialized == false )
            {
                initialized = true;
                Performance.Profile( "Database", ()=>{
                    stringtable = new StringTable();
                });
            }
		}
  }
}
