//
// GameJammers 2017
//

using blacktriangles;
using System.Collections.Generic;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    public class StringTable
    {
        // constants ///////////////////////////////////////////////////////////
        public static readonly string kDialogPath           = "story/dialog";
        public static readonly string kCharacterPath        = "story/colors";
        public static readonly string kCharacterPortraitPath= "characters/portraits";
        public const int kDialogHeaderSize                  = 12;
        public const int kCharacterHeaderSize               = 3;

        // types ///////////////////////////////////////////////////////////////
        public class DialogEntry
        {
            public string key                               = System.String.Empty;
            public string speaker                           = System.String.Empty;
            public string dialog                            = System.String.Empty;
        }

        public class CharacterEntry
        {
            public string id                                = System.String.Empty;
            public string displayName                       = System.String.Empty;
            public Sprite portrait                          = null;
            public Color color                              = Color.white;
        }

        // members /////////////////////////////////////////////////////////////
        private Dictionary<string,DialogEntry> dialog       = new Dictionary<string,DialogEntry>();
        private Dictionary<string,CharacterEntry> characters= new Dictionary<string,CharacterEntry>();

        // constructor / destructor ////////////////////////////////////////////
        public StringTable()
        {
            LoadDialog();
            LoadCharacterEntries();
        }

        // public methods //////////////////////////////////////////////////////
        public List<string> GetDialogIds()
        {
            List<string> result = new List<string>();
            foreach( string key in dialog.Keys )
            {
                result.Add( key );
            }
            return result;
        }

        public DialogEntry GetDialog( string key )
        {
            DialogEntry result = null;
            key = key.ToUpper();
            if( dialog.TryGetValue( key, out result ) == false )
            {
                result = null;
                DebugUtility.Warning( "Failed to load a dialog entry called " + key );
            }

            return result;
        }

        public CharacterEntry GetCharacter( string key )
        {
            CharacterEntry result = null;
            key = key.ToUpper();
            if( characters.TryGetValue( key, out result ) == false )
            {
                result = null;
                DebugUtility.Warning( "Failed to load a character entry called " + key );
            }

            return result;
        }

        // private methods /////////////////////////////////////////////////////
        private void LoadDialog()
        {
            TextAsset tableraw = Resources.Load<TextAsset>(kDialogPath);
            string[] lines = tableraw.text.Split( '\n' );

            // skip the header section
            for( int i = kDialogHeaderSize; i < lines.Length; ++i )
            {
                if( lines[i].Length > 1 )
                {
                    string[] columns = lines[i].Split( '\t' );
                    if( columns.Length < 3 )
                    {
                        DebugUtility.Warning( "Parse error in dialog file on line (" + i.ToString() + "): " + lines[i] + ", " + lines[i].Length.ToString() );
                    }
                    else
                    {
                        DialogEntry entry = new DialogEntry();
                        entry.key = columns[0];
                        entry.speaker = columns[1];
                        entry.dialog = columns[2];
                        dialog[ entry.key ] = entry;
                    }
                }
            }
        }

        public void LoadCharacterEntries()
        {
            TextAsset tableraw = Resources.Load<TextAsset>(kCharacterPath);
            string[] lines = tableraw.text.Split( '\n' );

            for( int i = kCharacterHeaderSize; i < lines.Length; ++i )
            {
                if( lines[i].Length > 1 )
                {
                    string[] columns = lines[i].Split( '\t' );
                    if( columns.Length < 3 )
                    {
                        DebugUtility.Warning( "Parse error in character file on line (" + i.ToString() + "): " + lines[i] + ", " + lines[i].Length.ToString() );
                    }
                    else
                    {
                        CharacterEntry entry = new CharacterEntry();
                        entry.id = columns[0];
                        entry.displayName = columns[1];
                        entry.color = ColorExtension.HexToColor( columns[2] );
                        entry.portrait = Resources.Load<Sprite>(System.String.Format( "{0}/{1}", kCharacterPortraitPath, entry.id ));
                        characters[entry.id] = entry;
                    }
                }
            }
        }
    }
}
