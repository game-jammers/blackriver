//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using blacktriangles;
using blacktriangles.Input;
using System.Collections.Generic;

namespace GameJammers.BlackRiver
{
	public class GameSceneController
        : SceneController
    {
        // members /////////////////////////////////////////////////////////////
        public NavigationMesh navigationMesh                = null;
        public InventoryPanel inventoryPanel                = null;

        public List<InventoryItem.Type> unlockSequence      = new List<InventoryItem.Type>();
        public Interactable interactOnSuccess               = null;
        public Interactable interactOnFail                  = null;

        private List<InventoryItem.Type> performedSequence  = new List<InventoryItem.Type>();

        // public methods //////////////////////////////////////////////////////
        public void UseInventoryItem( InventoryItem.Type type )
        {
            if( unlockSequence.Contains( type ) )
            {
                if( performedSequence.Contains( type ) == false )
                {
                    performedSequence.Add( type );
                    if( performedSequence.Count == unlockSequence.Count )
                    {
                        interactOnSuccess.Interact();
                    }
                }
            }
            else
            {
                if( interactOnFail != null )
                    interactOnFail.Interact();

                performedSequence.Clear();
            }
        }

    }
}
