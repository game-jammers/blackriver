//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using blacktriangles;

namespace blacktriangles.Tools2d
{
	public class AnimationGenerator
		: EditorWindow
	{
		// constants //////////////////////////////////////////////////////////
		private const string kMenuPath 							= "Tools/blacktriangles/2d/Animation Generator";
		private const string kTitle								= "Animation Generator";

		// members ////////////////////////////////////////////////////////////
		private SpritesheetLayout layout						= null;
		private GameObject prefab								= null;

		private Vector2 previewScrollPos						= Vector2.zero;
		private float previewAnimFPS							= 60f;
		private double lastUpdateTime							= 0;

		private GUIStyle buttonNormal							= null;

		// constructor / initializer //////////////////////////////////////////
		[MenuItem( kMenuPath )]
		public static void OpenWindow()
		{
			AnimationGenerator window = EditorWindow.GetWindow<AnimationGenerator>() as AnimationGenerator;
			window.titleContent = new GUIContent( kTitle );
			window.Initialize();
		}

		private void Initialize()
		{
			layout = null;
			buttonNormal = null;
		}

		// unity callbacks ////////////////////////////////////////////////////
		protected virtual void OnEnable()
		{
			Initialize();
		}

		protected virtual void OnGUI()
		{
			RefreshGUIStyles();

			EditorGUILayout.HelpBox( "The GUI for this window is a work in progress.", MessageType.Warning );

			GUILayout.BeginVertical( GUI.skin.box );
				if( GUILayout.Button( "Load" ) )
				{
					layout = SpritesheetLayout.CreateWithDialog();
				}

				if( layout != null )
				{
					GUILayout.Label( layout.name );
					layout.spritesheet = EditorGUILayout.ObjectField( "Spritesheet", layout.spritesheet, typeof( Texture2D ), false ) as Texture2D;
					prefab = EditorGUILayout.ObjectField( "Character Body", prefab, typeof( GameObject ), false ) as GameObject;
					if( prefab != null )
					{
						SpriteRenderer[] children = prefab.transform.GetComponentsInChildren<SpriteRenderer>();
						foreach( SpriteRenderer child in children )
						{
							GUILayout.Label( child.name );
						}
					}
				}
			GUILayout.EndVertical();
			if( layout != null && layout.spritesheet != null )
			{
				GUILayout.BeginVertical( GUI.skin.box );
					previewScrollPos = GUILayout.BeginScrollView( previewScrollPos );
						foreach( SpritesheetAnimation anim in layout.animations )
						{
							GUIContent animSpriteContent = new GUIContent( "[ERROR]" );
							SpritesheetAnimationFrame previewFrame = anim.GetFrameAtTime( (float)(EditorApplication.timeSinceStartup % anim.duration), true );
							if( previewFrame != null )
							{
								Texture2D previewTex = layout.GetTextureAt( previewFrame.index );
								if( previewTex != null )
								{
									animSpriteContent = new GUIContent( previewTex );
								}
							}

							GUILayout.BeginHorizontal();
								GUILayout.Label( animSpriteContent, GUIStyles.buttonNormal, GUILayout.Height( 50f ), GUILayout.Width( 50f ) );
								GUILayout.Label( anim.name );
							GUILayout.EndHorizontal();
						}
					GUILayout.EndScrollView();
				GUILayout.EndVertical();

				if( GUILayout.Button( "Create" ) )
				{
					CreateAnimations();
				}
			}
		}

		protected virtual void Update()
		{
			double now = EditorApplication.timeSinceStartup;
			if( now - lastUpdateTime > 1f/previewAnimFPS )
			{
				lastUpdateTime = now;
				Repaint();
			}
		}

		// gui callbacks //////////////////////////////////////////////////////
		private void RefreshGUIStyles()
		{
			GUIStyles.Initialize();
			GUIStyles.OnGUI();

			if( buttonNormal == null )
				buttonNormal = GUIStyles.buttonNormal;
		}

		private void CreateAnimations()
		{
			if( layout != null && layout.spritesheet != null )
			{
				string outputDir = EditorUtility.SaveFolderPanel( "Select Animation Output Directory", System.String.Empty, System.String.Empty );
				if( System.String.IsNullOrEmpty( outputDir ) == false )
				{
					FileUtility.EnsureDirectoryExists( outputDir );
					outputDir = FileUtility.MakePathRelativeToAssetDir( outputDir );

					EditorCurveBinding binding = new EditorCurveBinding();
					binding.type = typeof( SpriteRenderer );
					binding.path = System.String.Empty;
					binding.propertyName = "m_Sprite";

					layout.CreateAnimations( binding, outputDir );
				}
			}
		}

	}
}
