//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using blacktriangles;

namespace blacktriangles.Tools2d
{
	public class SpritesheetAnimationFrame
		: IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////
		public int index										= -1;
		public float duration									= 0f;

		// public methods /////////////////////////////////////////////////////
		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["index"] = index;
			result["duration"] = duration;
			return result;
		}

		public void FromJson( JsonObject json )
		{
			index = json.GetField<int>( "index" );
			duration = json.GetField<float>( "duration" );
		}
	}
}
