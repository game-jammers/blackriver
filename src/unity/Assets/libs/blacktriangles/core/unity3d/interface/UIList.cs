//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
	public class UIList
		: UIElement
		, IEnumerable
    	, IEnumerable<UIListElement>
	{
	    // members ////////////////////////////////////////////////////////////
        [SerializeField] ScrollRect scrollRect                  = null;
	    [SerializeField] UIListElement itemPrefab               = null;
	    [SerializeField] UIElement content                      = null;
	    [SerializeField] UIListDetail detailPanel               = null;
	    [SerializeField] Image expandToggle                     = null;
	    [SerializeField] Sprite expandedIcon                    = null;
	    [SerializeField] Sprite collapsedIcon                   = null;

	    public bool isExpanded                                  { get; private set; }
		public int selectedItemIndex							{ get { return GetSelectedItemIndex(); } set { SelectItem( value ); } }
	    public UIListElement selectedItem                       { get { return GetSelectedItem(); } set { SelectItem( value ); } }
	    public System.Object selectedItemData                   { get { return GetSelectedItemData(); } set { SelectItemFromData( value ); } }

	    private List<System.Object> itemData                    = new List<System.Object>();
	    private List<UIListElement> itemPanels                  = new List<UIListElement>();

	    // public methods /////////////////////////////////////////////////////
	    public override void Refresh()
	    {
			base.Refresh();
			if( expandToggle )
			{
				expandToggle.sprite = isExpanded ? expandedIcon : collapsedIcon;
			}

			itemPanels.SetLength(
			    itemData.Count
			  , (index)=>{
			    	UIListElement newItem = Instantiate( itemPrefab );
					newItem.Initialize( this );
			    	newItem.transform.SetParent( content.transform );
			    	return newItem;
			  }
			  , (item,index)=>{
			    	Destroy( item.gameObject );
			  }
			);

			for( int i = 0; i < itemData.Count; ++i )
			{
				itemPanels[i].Refresh( itemData[i], i );
			}

			if( detailPanel )
			{
				detailPanel.Refresh( selectedItem );
			}
	    }

        public void ShowSelectedItem()
        {
            UIListElement sel = GetSelectedItem();
            float yOffset = sel.rectTransform.localPosition.y;
            float yScroll = 1.0f + (yOffset / content.rectTransform.rect.height);
            scrollRect.verticalNormalizedPosition = yScroll;
        }

	    // public access methods //////////////////////////////////////////////////
		public int GetIndex( UIListElement selection )
		{
			return itemPanels.IndexOf( selection );
		}

		public int GetIndexFromData( System.Object selection )
		{
			return itemData.IndexOf( selection );
		}

        public UIListElement GetItem( int index )
        {
            if( itemPanels.IsValidIndex(index) == false ) return null;
            return itemPanels[index];
        }

		public void SelectItem( int index )
		{
			if( selectedItemIndex != index )
			{
				UIListElement ele = GetItem( index );
                SelectItem( ele );
			}
		}

	    public void SelectItem( UIListElement selection )
	    {
			if( selection != null )
            {
                selection.isSelected = true;
                Refresh();
            }
	    }

	    public void SelectItemFromData( System.Object selection )
	    {
			int index = GetIndexFromData( selection );
			SelectItem( index );
	    }

	    public UIListElement GetItemFromData( System.Object data )
	    {
			UIListElement result = null;

			int i = itemData.IndexOf( data );
			if( i >= 0 )
			{
				result = itemPanels[i];
			}

			return result;
	    }

	    public void SetData( IEnumerable data )
	    {
			itemData.Clear();
			foreach( System.Object obj in data )
			{
				if( obj != null )
				{
  					itemData.Add( obj );
				}
			}
			Refresh();
	    }

	    // ui callbacks ///////////////////////////////////////////////////////
	    public void ToggleExpanded()
	    {
			isExpanded = !isExpanded;
			Refresh();
	    }

		// private methods ////////////////////////////////////////////////////
        private int GetSelectedItemIndex()
        {
            int result = -1;
            UIListElement sel = GetSelectedItem();
            if( sel != null )
            {
                result = sel.index;
            }
            return result;
        }

		private UIListElement GetSelectedItem()
		{
			foreach( UIListElement item in itemPanels )
            {
                if( item.isSelected ) return item;
            }

            return null;
		}

	    private System.Object GetSelectedItemData()
	    {
			UIListElement item = GetSelectedItem();
            if( item != null ) return item.data;
            return null;
	    }

	    // operators //////////////////////////////////////////////////////////
	    IEnumerator IEnumerable.GetEnumerator()
	    {
			return itemPanels.GetEnumerator();
	    }

	    public IEnumerator<UIListElement> GetEnumerator()
	    {
			return itemPanels.GetEnumerator();
	    }
	}
}
