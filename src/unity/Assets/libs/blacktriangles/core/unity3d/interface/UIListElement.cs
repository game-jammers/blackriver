//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEngine.UI;
using blacktriangles;

using System.Collections;
using System.Collections.Generic;

namespace blacktriangles
{
	public class UIListElement
		: UIElement
	{
	    // members ////////////////////////////////////////////////////////////
	    public System.Object data								{ get; private set; }
		public UIList parent									{ get; private set; }
		public int index										{ get; private set; }
        public virtual bool isSelected                          { get; set; }

		// constructor / initializer //////////////////////////////////////////
		public virtual void Initialize( UIList _parent )
		{
			parent = _parent;
		}

	    // public methods /////////////////////////////////////////////////////
	    public void Refresh( System.Object _data, int _index )
	    {
			data = _data;
			index = _index;
			state.highlighted.active = isSelected;
			Refresh();
		}
	}
}
