//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using blacktriangles;

namespace blacktriangles
{
	public abstract class btCamera
    	: MonoBehaviour
	{
		// types //////////////////////////////////////////////////////////////
	    public enum UpdateType
	    {
			Update,
			FixedUpdate,
			LateUpdate,
			Manual
	    }

	    // members ////////////////////////////////////////////////////////////
	    public Camera unityCamera							= null;
		public UpdateType updateType                        = UpdateType.Manual;
        public Skybox skybox                                { get { return GetSkybox(); } }
        public LayerMask selectable;

        private Skybox _skybox                              = null;

		// camera callbacks ///////////////////////////////////////////////////////
	    public virtual void UpdateCamera()
	    {
	    }

	    // unity callbacks ////////////////////////////////////////////////////////
	    protected virtual void Update()
	    {
			if( updateType == UpdateType.Update )
			{
				UpdateCamera();
			}
	    }

	    protected virtual void FixedUpdate()
	    {
			if( updateType == UpdateType.FixedUpdate )
			{
				UpdateCamera();
			}
	    }

	    protected virtual void LateUpdate()
	    {
			if( updateType == UpdateType.LateUpdate )
			{
				UpdateCamera();
			}
	    }

	    //// utilities ////////////////////////////////////////////////////////////
	    public Vector3 WorldToScreen( Vector3 worldPos )
	    {
			return unityCamera.WorldToScreenPoint( worldPos );
	    }

        public Ray ScreenToRay( Vector3 screenPos )
        {
            return unityCamera.ScreenPointToRay( screenPos );
        }

        public Rect GetScreenBounds( Plane groundPlane )
        {

            Vector3 bottomLeft = ScreenToWorld( Vector3.zero, groundPlane );
            Vector3 topRight = ScreenToWorld( new Vector3( Screen.width, Screen.height, 0.0f ), groundPlane );
            Rect result = Rect.MinMaxRect( bottomLeft.x, bottomLeft.y, topRight.x, topRight.y );
            return result;
        }

        public Vector3 ScreenToWorld( Vector3 screenPos, Vector3 groundNormal )
        {
            Plane groundPlane = new Plane( groundNormal, Vector3.zero );
            return ScreenToWorld( screenPos, groundPlane );
        }

	    public Vector3 ScreenToWorld( Vector3 screenPos, Plane groundPlane )
		{
			return ScreenToWorld( unityCamera, screenPos, groundPlane );
		}

        public static Vector3 ScreenToWorld( Camera unityCam, Vector3 screenPos, Plane groundPlane )
        {
            Vector3 result = Vector3.zero;

			if( unityCam != null )
			{
				Ray ray = unityCam.ScreenPointToRay( screenPos );

				float distance = 0f;
				groundPlane.Raycast( ray, out distance );

				result = ray.GetPoint( distance );
			}

			return result;
        }

		public Vector3 ScreenToWorld( Vector3 screenPos )
		{
			return ScreenToWorld( screenPos, Vector3.up );
		}

        public bool MousePick( Vector3 screenPos, out RaycastHit result, float distance = Mathf.Infinity )
        {
            return MousePick( screenPos, selectable, distance, out result );
        }

	    public bool MousePick( Vector3 screenPos, LayerMask mask, float distance, out RaycastHit result )
	    {
            result = new RaycastHit();
			bool success = false;
			if( unityCamera != null )
			{
				Ray ray = unityCamera.ScreenPointToRay( screenPos );
				success = Physics.Raycast( ray.origin, ray.direction, out result, distance, mask );
			}

			return success;
		}

        public ComponentType MousePick<ComponentType>( Vector3 screenPos, float distance = Mathf.Infinity )
            where ComponentType: MonoBehaviour
        {
            ComponentType result = null;
            RaycastHit hit;
            if( MousePick( screenPos, out hit, distance ) )
            {
                result = hit.collider.gameObject.GetComponent<ComponentType>();
            }
            return result;
        }

        public RaycastHit2D MousePick2D( Vector3 screenPos, LayerMask mask, float distance )
	    {
            RaycastHit2D result = new RaycastHit2D();
			if( unityCamera != null )
			{
				Ray ray = unityCamera.ScreenPointToRay( screenPos );
				result = Physics2D.Raycast( ray.origin, ray.direction, distance, mask );
			}

			return result;
		}


        public ComponentType MousePick2D<ComponentType>( Vector3 screenPos, float distance = Mathf.Infinity )
            where ComponentType : MonoBehaviour
        {
            ComponentType result = null;
            RaycastHit2D hit = MousePick2D( screenPos, selectable, distance );
            if( hit.collider != null )
            {
                result = hit.collider.gameObject.GetComponent<ComponentType>();
            }
            else
            {
                Debug.Log( "No pick?" );
            }

            return result;
        }

        // private methods /////////////////////////////////////////////////////
        private Skybox GetSkybox()
        {
            if( _skybox == null )
            {
                _skybox = unityCamera.GetComponent<Skybox>();
            }

            return _skybox;
        }
	}
}
