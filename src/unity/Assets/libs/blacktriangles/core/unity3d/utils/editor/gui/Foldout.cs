//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Using modified bezier curve drawing code originally written by Linusmartensson
// http://forum.unity3d.com/threads/drawing-lines-in-the-editor.71979/
//
//=============================================================================

using System.Collections.Generic;
using System.Reflection;
using UnityEditor;
using UnityEngine;

namespace blacktriangles
{
	public static partial class btGui
	{
        //
        // foldout serialize property ##########################################
        //
        public static Rect Foldout( Rect position, ref bool expanded, GUIContent label )
        {
            Texture2D icon = expanded ? EditorIcons.CollapseVertical : EditorIcons.ExpandVertical;

            // draw button
            expanded = GUI.Button( position, label, GUIStyles.header ) ? !expanded : expanded;

            // draw expand icon
            Rect iconRect = new Rect( position.x, position.y, kRemoveButtonSize, kRemoveButtonSize );
            GUI.Label( iconRect, icon, GUIStyles.buttonNoBorder );



            return new Rect( position.x, position.y + position.height, position.width, position.height );
        }

        //
        // foldout layout ######################################################
        //
		public static bool Foldout( bool expanded, string label, Texture2D icon )
		{
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label( icon, GUIStyles.buttonNoBorder, GUILayout.Width( kRemoveButtonSize ), GUILayout.Height( kRemoveButtonSize ) );
				bool result = Foldout( expanded, label );
			EditorGUILayout.EndHorizontal();

			return result;
		}

		public static bool Foldout( bool expanded, string label )
		{
			Texture2D icon = expanded ? EditorIcons.CollapseVertical : EditorIcons.ExpandVertical;
			EditorGUILayout.BeginHorizontal();
				GUILayout.Label( label, GUIStyles.buttonNoBorder );
				if( GUILayout.Button( icon, GUIStyles.buttonNoBorder, GUILayout.Height( kRemoveButtonSize ), GUILayout.Width( kRemoveButtonSize ) ) )
					expanded = !expanded;
			EditorGUILayout.EndHorizontal();

			return expanded;
		}
	}
}
