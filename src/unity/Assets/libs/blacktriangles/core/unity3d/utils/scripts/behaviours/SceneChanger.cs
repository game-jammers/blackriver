//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace blacktriangles
{
	public class SceneChanger
		: MonoBehaviour
	{
		// public methods //////////////////////////////////////////////////////
		public void ChangeScene( int sceneIndex )
        {
            SceneManager.LoadScene( sceneIndex );
        }

        public void ChangeScene( string sceneName )
        {
            SceneManager.LoadScene( sceneName );
        }
	}
}
