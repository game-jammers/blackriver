//=============================================================================
//
// (C) BLACKTRIANGLES 2016
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

#ifndef BTLIGHTUTIL_CGINC
#define BTLIGHTUTIL_CGINC

// variables ///////////////////////////////////////////////////////////////////
float _Banding;
float _Growth;

float3 _Color0;
float3 _Color1;
float3 _Color2;
float3 _Color3;
float3 _Color4;

float calculateLightPower( float attenuation, float3 worldPos, float3 normalDir )
{
    float3 lightDir = normalize(lerp(_WorldSpaceLightPos0.xyz, _WorldSpaceLightPos0.xyz - worldPos.xyz,_WorldSpaceLightPos0.w));
    float power = max( 0, dot( lightDir, normalDir ) );

    attenuation = floor( attenuation * _Banding ) / _Banding;
    return attenuation * power;
}

float3 colorizeBasedOnLightPower( float power )
{
    return float3( power, power, power );
    if( power < 0.002 )
    {
        return _Color0;
    }
    else if( power < 0.004 )
    {
        return _Color1;
    }
    else if( power < 0.006 )
    {
        return _Color2;
    }
    else if( power < 0.008 )
    {
        return _Color3;
    }

    return _Color4;
}

#endif
