//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Inspired by a Answers post by It3ration
//	http://answers.unity3d.com/questions/489942/how-to-make-a-readonly-property-in-inspector.html
//=============================================================================

using UnityEngine;
using UnityEditor;
using System.Collections;

namespace blacktriangles
{
  public class InspectorReadOnlyFieldAttribute : PropertyAttribute {}

  [CustomPropertyDrawer(typeof(InspectorReadOnlyFieldAttribute))]
  public class InspectorReadOnlyFieldDrawer : PropertyDrawer
  {
     public override float GetPropertyHeight(SerializedProperty property,
                                             GUIContent label)
     {
         return EditorGUI.GetPropertyHeight(property, label, true);
     }

     public override void OnGUI(Rect position,
                                SerializedProperty property,
                                GUIContent label)
     {
         GUI.enabled = false;
         EditorGUI.PropertyField(position, property, label, true);
         GUI.enabled = true;
     }
  }
}
