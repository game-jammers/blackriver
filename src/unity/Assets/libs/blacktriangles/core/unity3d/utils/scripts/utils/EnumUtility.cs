//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

﻿using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
	public static class EnumUtility
	{
    public static int Count<EnumType>()
    {
      return System.Enum.GetValues( typeof( EnumType ) ).Length;
    }

    public static EnumType Random<EnumType>()
    {
      int count = Count<EnumType>();
      return (EnumType)System.Convert.ChangeType( btRandom.Range( 0, count ), typeof( EnumType ) );
    }
  }
}
