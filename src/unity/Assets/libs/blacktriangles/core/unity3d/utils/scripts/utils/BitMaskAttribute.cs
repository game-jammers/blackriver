//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
// Inspired by a Answers post by Bunny83
//	http://answers.unity3d.com/questions/393992/custom-inspector-multi-select-enum-dropdown.html
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
	public class BitMaskAttribute
		: PropertyAttribute
	{
		// members ////////////////////////////////////////////////////////////
		public System.Type propType;

		// constructor / initializer //////////////////////////////////////////
		public BitMaskAttribute(System.Type aType)
		{
			propType = aType;
		}
	}
}
