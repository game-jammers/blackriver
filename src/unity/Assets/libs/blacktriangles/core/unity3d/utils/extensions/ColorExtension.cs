//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
	public static class ColorExtension
	{
		public static string ToHexString( this Color self )
		{
			byte[] bytes = { self.r.ToByte(), self.g.ToByte(), self.b.ToByte() };
			return "#" + System.BitConverter.ToString(bytes).Replace("-","");
		}

        public static Color HexToColor(string hex)
        {
        	byte r = byte.Parse(hex.Substring(1,2), System.Globalization.NumberStyles.HexNumber);
        	byte g = byte.Parse(hex.Substring(3,2), System.Globalization.NumberStyles.HexNumber);
        	byte b = byte.Parse(hex.Substring(5,2), System.Globalization.NumberStyles.HexNumber);
        	return new Color32(r,g,b, 255);
        }
	}
}
