//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles.Input
{
	public class InputState
	{
		// members ////////////////////////////////////////////////////////////
		public List<InputAction> actions						{ get; private set; }
        public TimestampedValue<bool> wasKeyPressed             = new TimestampedValue<bool>();
        public TimestampedValue<bool> isKeyDown                 = new TimestampedValue<bool>();
        public TimestampedValue<bool> wasKeyReleased            = new TimestampedValue<bool>();

        // constructor / initializer //////////////////////////////////////////
        public InputState()
        {
            actions = new List<InputAction>();
        }

		// public methods /////////////////////////////////////////////////////
        public void Update()
        {
            GetKeyDown();
            GetKey();
            GetKeyUp();
        }

        public float GetAxis()
		{
			float result = 0f;
			foreach( InputAction action in actions )
			{
				result += action.GetAxis();
			}

			return result;
		}

		public bool GetKeyDown()
		{
			bool result = false;
			foreach( InputAction action in actions )
			{
                result = result || action.GetKeyDown();
                if( result ) break;
			}

            wasKeyPressed.val = result;
			return result;
		}

		public bool GetKey()
		{
			bool result = false;
			foreach( InputAction action in actions )
			{
				result = result || action.GetKey();
				if( result ) break;
			}

            isKeyDown = result;
			return result;
		}

		public bool GetKeyUp()
		{
			bool result = false;
			foreach( InputAction action in actions )
			{
				result = result || action.GetKeyUp();
				if( result ) break;
			}

            wasKeyReleased = result;
			return result;
		}
	}
}
