//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using System.Collections.Generic;
using System.Linq;
using UnityEngine;

namespace blacktriangles.Graphics
{
    public static class MarchingCubes
    {
        public static List<Vector3> March( BoxCollider area, float gridSize, IEnumerable<SphereCollider> spheres )
        {
            HashSet<Vector3> result = new HashSet<Vector3>();
            for( float x = area.bounds.min.x; x < area.bounds.max.x; x += gridSize )
            {
                for( float y = area.bounds.min.y; y < area.bounds.max.y; y += gridSize )
                {
                    for( float z = area.bounds.min.z; z < area.bounds.max.z; z += gridSize )
                    {
                        Vector3 point = new Vector3( x, y, z );
                        foreach( SphereCollider ball in spheres )
                        {
                            if( (ball.transform.position-point).sqrMagnitude < ball.radius )
                            {
                                result.Add( point );
                            }
                        }
                    }
                }
            }
            return result.ToList<Vector3>();
        }
    }
}
