//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using System;

namespace blacktriangles.Network
{
    [Serializable]
    public class ConnectionConfig
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public string hostname                              = "localhost";
        public int port                                     = 30000;

        // constructor / initializer ///////////////////////////////////////////
        public ConnectionConfig()
        {
        }

        public ConnectionConfig( string _hostname, int _port )
        {
            hostname = _hostname;
            port = _port;
        }

        // IJsonSerializable ///////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            JsonObject json = new JsonObject();
            json["hostname"] = hostname;
            json["port"] = port;
            return json;
        }

        public void FromJson( JsonObject json )
        {
            hostname = json.GetField<string>("hostname");
            port = json.GetField<int>("port");
        }
    };
}
