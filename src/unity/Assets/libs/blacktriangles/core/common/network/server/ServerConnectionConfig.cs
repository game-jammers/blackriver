//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;

namespace blacktriangles.Network
{
    [System.Serializable]
    public struct ServerConnectionConfig
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public int bufferSize;

        // constructor / destructor ////////////////////////////////////////////
        public ServerConnectionConfig( int _bufferSize )
        {
            bufferSize = _bufferSize;
            DebugUtility.Assert( bufferSize > 0, "Attempting to create a ServerConnectionConfig with a buffer size of 0 or less" );
        }

        public static ServerConnectionConfig Default()
        {
            ServerConnectionConfig result = new ServerConnectionConfig();
            result.bufferSize = 512;
            return result;
        }

        // public methods //////////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            JsonObject json = new JsonObject();
            json["bufferSize"] = bufferSize;
            return json;
        }

        public void FromJson( JsonObject json )
        {
            bufferSize = json.GetField<int>("bufferSize");
        }
    }
}
