//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;

namespace blacktriangles.Network
{
	public enum ActionErrorCode
	{
		NONE
	  , BADPARAMS		// The request came with invalid parameters
	  , NOAUTH			// You are not authorized to make this request
	  , NOAVAIL			// Resources are not available
	  , VALERR			// Validation error
	  , GENERR			// A general error, report this to me so I can make a more specific one
	}
}
