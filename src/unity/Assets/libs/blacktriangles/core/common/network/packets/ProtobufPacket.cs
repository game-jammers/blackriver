//=============================================================================
//
// (C) BLACKTRIANGLES 2014
// http://www.blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;
using blacktriangles.Network;
using ProtoBuf;
using System.IO;
using System;

namespace blacktriangles.Network
{
    public abstract class BaseProtobufPacket
        : Packet
    {
        // Packet //////////////////////////////////////////////////////////////
        public virtual void Decode<T>( RawPacket packet )
            where T : BaseProtobufPacket
        {
            bool streamWritable = false;
            MemoryStream stream = new MemoryStream( packet.payload.bytes, streamWritable );
            T self = this as T;
            Serializer.Merge<T>(stream, self);
        }

        public override RawPacket Encode()
        {
            MemoryStream stream = new MemoryStream();
            Serializer.Serialize( stream, this );
            NetworkBuffer payload = new NetworkBuffer( stream.ToArray() );
            return new RawPacket( id, payload );
        }
    }

    public abstract class ProtobufPacket<T>
        : BaseProtobufPacket
        where T: BaseProtobufPacket
    {
        public override void Decode(RawPacket packet)
        {
            Decode<T>(packet);
        }
    }
}
