//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using blacktriangles;

namespace blacktriangles.Network
{
    [System.Serializable]
    public struct SocketServerConfig
        : IJsonSerializable
    {
        // members /////////////////////////////////////////////////////////////
        public int port;
        public int bufferSize;
        public ServerConnectionConfig connConfig;

        // constructor / initializer ///////////////////////////////////////////
        public SocketServerConfig( int _port, int _bufferSize, ServerConnectionConfig _connConfig )
        {
            port = _port;
            bufferSize = _bufferSize;
            connConfig = _connConfig;
            DebugUtility.Assert( bufferSize > 0, "Attempting to create a SocketServerConfig with a buffer size of 0 or less" );
        }

        public SocketServerConfig( string json )
            : this()
        {
            FromJson( JsonObject.FromString(json) );
        }

        public static SocketServerConfig Default()
        {
            SocketServerConfig result = new SocketServerConfig();
            result.port = 30000;
            result.bufferSize = 512;
            result.connConfig = ServerConnectionConfig.Default();
            return result;
        }

        // public methods //////////////////////////////////////////////////////
        public JsonObject ToJson()
        {
            JsonObject json = new JsonObject();
            json["port"] = port;
            json["bufferSize"] = bufferSize;
            json["connConfig"] = connConfig;
            return json;
        }

        public void FromJson( JsonObject json )
        {
            port = json.GetField<int>("port");
            bufferSize = json.GetField<int>("bufferSize");
            connConfig = json.GetField<ServerConnectionConfig>("connConfig");
        }
    }
}
