//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using System.Collections.Generic;

namespace blacktriangles
{
	public static class RectExtension
	{
        public static Vector3[] ToVertices( this Rect self )
        {
            Vector3[] result = new Vector3[4];
            result[0] = new Vector3( self.min.x, self.min.y, 0.0f );
            result[1] = new Vector3( self.max.x, self.min.y, 0.0f );
            result[2] = new Vector3( self.max.x, self.max.y, 0.0f );
            result[3] = new Vector3( self.min.x, self.max.y, 0.0f );
            return result;
        }

        public static Vector2 ClosestPoint( this Rect self, Vector2 point )
        {
            float r = self.x + self.width;
            float b = self.y + self.height;

            float x = Mathf.Clamp( point.x, self.x, r );
            float y = Mathf.Clamp( point.y, self.y, b );

            float dl = Mathf.Abs( x - self.x );
            float dr = Mathf.Abs( x - r );
            float dt = Mathf.Abs( y - self.y );
            float db = Mathf.Abs( y - b );

            float min = Mathf.Min( dl, Mathf.Min( dr, Mathf.Min( dt, db ) ) );

            if( min == dt ) return new Vector2( x, self.y );
            if( min == db ) return new Vector2( x, b );
            if( min == dl ) return new Vector2( self.x, y );
            return new Vector2( r, y );
        }
    }
}
