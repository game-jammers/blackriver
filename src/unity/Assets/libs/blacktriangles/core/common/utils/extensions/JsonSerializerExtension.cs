//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
    // Vector3 ////////////////////////////////////////////////////////////////
    public static class Vector3JsonExtension
    {
        public static JsonObject ToJson( this Vector3 self )
        {
            JsonObject result = new JsonObject();
            result["x"] = self.x;
            result["y"] = self.y;
            result["z"] = self.z;
            return result;
        }

        public static Vector3 FromJson( JsonObject json )
        {
            if( json == null ) return default(Vector3);
            Vector3 result = Vector3.zero;
            result.x = json.GetField<float>("x");
            result.y = json.GetField<float>("y");
            result.z = json.GetField<float>("z");
            return result;
        }
    }

    // Vector 2////////////////////////////////////////////////////////////////
    public static class Vector2JsonExtension
    {
        public static JsonObject ToJson( this Vector2 self )
        {
            JsonObject result = new JsonObject();
            result["x"] = self.x;
            result["y"] = self.y;
            return result;
        }

        public static Vector2 FromJson( JsonObject json )
        {
            if( json == null ) return default(Vector2);
            Vector2 result = Vector2.zero;
            result.x = json.GetField<float>("x");
            result.y = json.GetField<float>("y");
            return result;
        }
    }

    // Rect ///////////////////////////////////////////////////////////////////
    public static class RectJsonExtension
    {
        public static JsonObject ToJson( this Rect self )
        {
            JsonObject result = new JsonObject();
            result["position"] = self.position;
            result["size"] = self.size;
            return result;
        }

        public static Rect FromJson( JsonObject json )
        {
            if( json == null ) return default(Rect);
            Vector2 pos = json.GetField<Vector2>( "position" );
            Vector2 size = json.GetField<Vector2>( "size" );
            Rect result = new Rect( pos.x, pos.y, size.x, size.y );
            return result;
        }
    }

    // Quaternion /////////////////////////////////////////////////////////////
    public static class QuaternionJsonExtension
    {
        public static JsonObject ToJson( this Quaternion self )
        {
            JsonObject json = new JsonObject();
            json["x"] = self.x;
            json["y"] = self.y;
            json["z"] = self.z;
            json["w"] = self.w;
            return json;
        }

        public static Quaternion FromJson( JsonObject json )
        {
            if( json == null ) return default(Quaternion);
            return new Quaternion(
                json.GetField<float>( "x" )
              , json.GetField<float>( "y" )
              , json.GetField<float>( "z" )
              , json.GetField<float>( "w" )
            );
        }
    }
}
