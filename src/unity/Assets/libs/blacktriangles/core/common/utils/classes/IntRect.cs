//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;

namespace blacktriangles
{
	[System.Serializable]
	public struct IntRect
		: IJsonSerializable
	{
		// members ////////////////////////////////////////////////////////////////
		public IntVec2 min;
		public IntVec2 max;
		public IntVec2 size										{ get { return new IntVec2( width, height ); } }
		public int width										{ get { return max.x - min.x; } set { max.x = min.x + value; } }
		public int height										{ get { return max.y - min.y; } set { max.y = min.y + value; } }

		// constructors ///////////////////////////////////////////////////////////
		public IntRect( int x, int y, int _width, int _height )
		{
			min = new IntVec2( x, y );
			max = new IntVec2( x + _width, y + _height );
		}

		public IntRect( Rect copy )
		{
			min = new IntVec2( copy.position );
			max = IntVec2.zero;
			width = (int)copy.width;
			height = (int)copy.height;
		}

		public IntRect( IntVec2 _min, IntVec2 _max )
		{
			min = _min;
			max = _max;
		}

		// public methods /////////////////////////////////////////////////////
		public bool Contains( IntVec2 point )
		{
			return point.x >= min.x && point.x <= max.x
				&& point.y >= min.y && point.y <= max.y;
		}

		// operators ///////////////////////////////////////////////////////////////
		public override string ToString()
		{
			return System.String.Format( "[x:{0} y:{1} width:{2} height:{3} maxx:{4} maxy: {5}]"
										, min.x
										, min.y
										, width
										, height
										, max.x
										, max.y
									);
		}

		public JsonObject ToJson()
		{
			JsonObject result = new JsonObject();
			result["min"] = min;
			result["max"] = max;
			return result;
		}

		public void FromJson( JsonObject json )
		{
			min = json.GetField<IntVec2>( "min" );
			max = json.GetField<IntVec2>( "max" );
		}
	}
}
