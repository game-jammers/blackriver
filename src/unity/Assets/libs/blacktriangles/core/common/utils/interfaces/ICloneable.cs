//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

namespace blacktriangles
{
	public interface IDeepCloneable<T>
	{
		T DeepClone();
	}

	public interface IShallowCloneable<T>
	{
		T ShallowClone();
	}
}
