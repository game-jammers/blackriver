Narrative Guide																	
D_NAME_SCEN_####				This is an example string.													
																	
				Player Home	City Courtyard	City Kennick's home	City Landfill	City Street	City Workshop	Old Caravan	Forest	Waterfall	Hills/Plains	Village Main	Village 'B'	Trade Road Main	Trade Road 'B'
SCENES				PHOM	CCRT	CKEN	CLND	CSTR	CWRK	OCAR	FRST	WTRF	HILL	VILM	VILB	TRDM	TRDB
																	
CHARACTERS			Squirrel	Zache	Kennick	Lel	Tawni	Patrin	Khulai	Tobar	Babik	Bireli	Durriken	Ferka	Ucho	Charani	Teberi
RELATION			 	Player	Elder	Clan (m)										Clan (f)	
Abbr.			SQRL	ZACH	KENN	LLLL	TAWN	PATN	KHUL	TOBR	BABK	BRLI	DURR	FERK	UCHO	CHNI	TEBE
																	
CHARACTERS (cont.)			Zaphira	Tsura	Mumely	Mirigli	Kisaiya	Minditsi	Fawnie	Lajaria	Dika	Elophia	Jaelle	Saeira	Shelta	Narrated	Kennai
RELATION					Elder/Friend			Child								The Ancestors	
Abbr.			ZAPH	TSUR	MUME	MIRG	KISY	MIND	FAWN	LAJA	DIKA	LOPH	JAEL	SARA	SHLT	NARR	KNAI
STRING_ID	Speaker	String														Position	Location
																	
D_NARR_PHOM_0001	NARR	After 12 days of mourning, the Clan should have gathered together, bound by blood and tradition. \nWe would have smiled and laughed openly as we celebrated Granna's life and everything she meant to us.\n...but that's not what happened. \nOn the 13th day, we gathered... and we argued.														1	PHOM
																	
D_ZACH_CKEN_0003	ZACH	Kennick, you have to do something!														2	CKEN
D_KENN_CKEN_0004	KENN	If the Clan wants to argue, let them argue. I have no say in the matter.														2	CKEN
D_ZACH_CKEN_0005	ZACH	Families are leaving! Who will lead the Clan?														2	CKEN
D_KENN_CKEN_0006	KENN	This has never happened before! Without a daughter or a sister, there is no clear choice as to who takes Granna's place as Matron.\nIt's out of my hands.														2	CKEN
																	
D_MUME_PHOM_0007	MUME	Zache, I've been looking for you.															PHOM
D_ZACH_PHOM_0008	ZACH	I can't believe Granna's gone. What will we do without our Matron?															PHOM
D_MUME_PHOM_0009	MUME	The same we've always done, boy. The Ancestors will guide us, but only if we ask.															PHOM
D_ZACH_PHOM_0010	ZACH	I've tried! All around us, there is nothing but silence.															PHOM
D_MUME_PHOM_0011	MUME	Come now, it's not about words...surely you're old enough to know that.\nWhy do you think we play music <b>every</b> night we lay camp?															PHOM
																	
D_SHLT_CCRT_0030	SHLT	Hello, Zache.															CCRT
D_ZACH_CCRT_0031	ZACH	<b>Wha</b>...who are you?															CCRT
D_SHLT_CCRT_0032	SHLT																CCRT
D_ZACH_CCRT_0033	ZACH																CCRT
D_SHLT_CCRT_0034	SHLT																CCRT
D_ZACH_CCRT_0035	ZACH																CCRT
 																	
																	
																	
D_ZACH_WTRF_0013	ZACH	By the goddess, is that Zaphira? Sahara, What happened to her?															WTRF
D_SARA_WTRF_0014	SARA	We... *sniff* we were hunting 2 moons ago, everything was alright, until a huge black worm suddenly attacks mama.\nmama is strong, she was able to kill it, but the worm bit mama, she is ill and...\n*sniff*. She was fine yesterday but now... We don't want to lose mama but we don't know what to do.															WTRF
D_ZACH_WTRF_0015	ZACH	(A black worm? She must be talking about the Pelosi Snake, the deadliest snake in this forest).\nI remember Kennick was bitten too by a "black worm" hundreds of moons ago, granna cured him somehow.\nWhy don't we go see Kennick to get some information?															WTRF
D_TEBE_WTRF_0016	TEBE	We already went to see Kennick, he said that granna cured him with the resin of a magic plant called Kahn.															WTRF
D_ZACH_WTRF_0017	ZACH	The Kahn? I always thought the Kahn was just a myth Teberi , but If Kennick says it exists..															WTRF
D_SARA_WTRF_0018	SARA	Then it must be real. We asked Kennick if he knew where to find this plant, he said he didn't know, but he remembers granna using a note, this note, when he was sick.\nHe says it is a clue to find the plant.															WTRF
D_ZACH_WTRF_0019	ZACH	May I see it? \n"When the sound of the wind and the sound of the water becomes one, the path to the khan shall be revealed".\nThat's all it says.															WTRF
D_TEBE_WTRF_0020	TEBE	Yes, we searched everywhere looking for the khan but...															WTRF
D_SARA_WTRF_0021	SARA	We couldn't find it, we wanted to keep searching today but mama... *sniff* she is too ill, we are scared to leave her alone, none of us wants to leave.\nWe need your help Zach, you're the smartest in the clan, please help us.															WTRF
D_ZACH_WTRF_0022	ZACH	Kennai: Please don't let my mama die.															WTRF
D_ZACH_WTRF_0023	ZACH	Zach : Kennai, she won't die,I won't let her die, I promise by the goddess. We will save her.															WTRF
D_SARA_WTRF_0024	SARA	Thank you! Thank you so much, Zach.															WTRF
D_TEBE_WTRF_0025	TEBE	Thank you!															WTRF
D_KNAI_WTRF_0026	KNAI	Thank you, Zach.															WTRF
D_ZACH_WTRF_0027	ZACH	Zach: I'm taking the note with me, be careful kids and don't worry, I'll find the Kahn.															WTRF
D_SARA_WTRF_0028	SARA	Sahara: Thank you so much Zach, please be fast, I don't know how much mama is going to resist.															WTRF
D_SARA_WTRF_0029	SARA	Sahara: Please Zach, hurry, mama will die if you don't!															WTRF
																	
D_SQRL_CCRT_0035	SQRL	Hey, buddy. I'm a squirrel. 															
D_ZACH_WTRF_0036	ZACH	<b>Awwwwweeeessssommmmeeee!!!!!</b>															
D_SQRL_CCRT_0037	SQRL	...it's ok.															
D_ZACH_WTRF_0038	ZACH	...															
D_SQRL_CCRT_0039	SQRL	Got any nuts?															