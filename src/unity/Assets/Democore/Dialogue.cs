﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Dialogue : MonoBehaviour
{
    public string DialogId;
    public Sprite HeadSprite;
    public List<string> dialogueFlow = new List<string>();
    public string Name;
    public Dialogue FollowUp;
    public bool IsStart = false;
    public string DisplayName;
    public AudioSource Voice;
    public Color textColor = new Color(0, 0, 0, 255);
    public float tonePitch = 1f;
    public float MaxInteractDistance = 2.5f;

    void Awake()
    {
        if(IsStart)
        {
            InitializeDialogue(false);
        }
    }

    public DialogueManager InitializeDialogue(bool isFollow)
    {
        DialogueManager manager = gameObject.GetComponent<DialogueManager>();
        if( manager == null )
            manager = gameObject.AddComponent<DialogueManager>();

        manager.headSprite = HeadSprite;
        manager.dialogueFlow = dialogueFlow.ToArray();
        manager.Name = DisplayName;
        manager.Voice = Voice;
        manager.textColor = textColor;
        manager.tonePitch = tonePitch;
        manager.MaxInteractDistance = MaxInteractDistance;

        if (FollowUp)
        {
            DialogueManager followManager = FollowUp.InitializeDialogue(true);
            manager.Followup = followManager;
        }
        if(isFollow)
        {
            manager.IsInteractable = false;
        }
        return manager;
    }
}
