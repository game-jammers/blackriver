﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NoteSelector : MonoBehaviour {

    Image image;

	// Use this for initialization
	void Awake()
    {
        image = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update ()
    {
		
	}

    public void MouseEnter()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 1.0f);
    }

    public void MouseExit()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0.3f);
    }

    public void MouseClick()
    {
        image.color = new Color(image.color.r, image.color.g, image.color.b, 0.3f);
        this.transform.parent.gameObject.SetActive(false);
    }
}
