﻿
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSound : MonoBehaviour {
    public AudioSource Player;
    public AudioSource BackgroundMusic;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void StartNewGame()
    {
        SaveManager.NewGame("BoxChanForPresident");
        PlaySound();
    }

    public void PlaySound()
    {
        if (Player)
            Player.Play();

        if(BackgroundMusic)
            StartCoroutine(AudioFade.FadeOut(BackgroundMusic, 2.9f));
        StartCoroutine(LoadNextScene());
    }

    IEnumerator LoadNextScene()
    {
        yield return new WaitForSeconds(2.9f);
        SceneManager.LoadScene(1);
    }
}
