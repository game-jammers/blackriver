﻿using GameJammers.BlackRiver;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Collider2D))]
[System.Serializable]
public class Interactable : MonoBehaviour
{
    public enum Interaction { Interact, Listen, none };
    public Interaction LeftAction = Interaction.Interact;
    public Interaction RightAction = Interaction.Listen;
    public string Name = "Unknown";
    public string InteractText = "Unknown Interacttext";
    public string ListenText = "Unknown Listentext";
    public string UnlockKey = "";
    public bool IsInteractable = true;
    public float MaxInteractDistance = 2.5f;
    public List<string> FailMessage = new List<string>();

    private Texture2D NormalCursor = null;
    private Texture2D InteractOnlyCursor = null;
    private Texture2D ListeningOnlyCursor = null;
    private Texture2D InteractAndListeningCursor = null;
    private Texture2D NotInteractableCursor = null;

    private Character character = null;

    private bool IsCurserIn = false;
    private bool WasCursorDown = false;

    private GameObject HoverObjectNameText;
    private Text HoverNameText;


	// Use this for initialization
	public virtual void Awake ()
    {
        //SaveManager.LoadGame("myTest");
        //SaveManager.SetCurrentKey("akwuhdakuwd", false);

        //Make sure nobody fucks up the settings.
		if(LeftAction == Interaction.none && RightAction != Interaction.none)
        {
            LeftAction = RightAction;
            RightAction = Interaction.none;
        }
        if(LeftAction == Interaction.Listen && RightAction == Interaction.Interact)
        {
            LeftAction = Interaction.Interact;
            RightAction = Interaction.Listen;
        }
        if(LeftAction == RightAction)
        {
            RightAction = Interaction.none;
        }

        //Make sure the Collider is a Trigger.
        Collider2D interactCollider = GetComponent<Collider2D>();
        interactCollider.isTrigger = true;

        //Load the Cursors. null = default windows
        NormalCursor = null;
        InteractOnlyCursor = Resources.Load("Interact") as Texture2D;
        ListeningOnlyCursor = Resources.Load("Ear") as Texture2D;
        InteractAndListeningCursor = Resources.Load("Both") as Texture2D;
        NotInteractableCursor = null;

        HoverObjectNameText = GameObject.Find("HoverObjectNameText");
        if (HoverObjectNameText)
        {
            HoverNameText = HoverObjectNameText.GetComponent<Text>();
        }

        character = getCharacter();
    }

    void OnMouseEnter()
    {
        //Change cursor to thingy
        IsCurserIn = true;
        DoCursorChange(true);
    }
    void OnMouseOver()
    {
        //needed?
    }
    void OnMouseExit()
    {
        //Change cursor to default
        IsCurserIn = false;
        WasCursorDown = false;
        DoCursorChange(false);
        if (HoverObjectNameText)
        {
            HoverObjectNameText.transform.position = new Vector3(100000000, 0, 0);
        }
    }

    public void ActionPressed(int LeftOrRight)
    {
        if(IsInteractable && isUnlocked() && isCharacterInReach())
        {
            if (LeftOrRight == 0 /*left*/)
            {
                if (LeftAction == Interaction.Interact)
                    Interact();
                else if (LeftAction == Interaction.Listen)
                    Listen();
            }
            else if (LeftOrRight == 1/*Right*/)
            {
                if (RightAction == Interaction.Interact)
                    Interact();
                else if (RightAction == Interaction.Listen)
                    Listen();
            }
        }
        else if (!isUnlocked() && IsInteractable && isCharacterInReach())
        {
            WrongKey();
        }
    }

    public void DoCursorChange(bool isInside)
    {
        CursorMode cursorMode = CursorMode.Auto;
        Vector2 hotSpot = new Vector2(10, 13);
        if(isInside)
        {
            if(isUnlocked())
            {
                if (LeftAction == Interaction.Interact && RightAction == Interaction.none)
                {
                    Cursor.SetCursor(InteractOnlyCursor, hotSpot, cursorMode);
                }
                else if (LeftAction == Interaction.Listen && RightAction == Interaction.none)
                {
                    Cursor.SetCursor(ListeningOnlyCursor, hotSpot, cursorMode);
                }
                else if (LeftAction == Interaction.Interact && RightAction == Interaction.Listen)
                {
                    Cursor.SetCursor(InteractAndListeningCursor, hotSpot, cursorMode);
                }
            }
            else
            {
                Cursor.SetCursor(NotInteractableCursor, hotSpot, cursorMode);
            }
        }
        else
        {
            Cursor.SetCursor(NormalCursor, Vector2.zero, cursorMode);
        }
    }

    public virtual void Interact()
    {
        Debug.Log(Name);
        Debug.Log(InteractText);
    }

    public virtual void Listen()
    {
        Debug.Log(Name);
        Debug.Log(ListenText);
    }

    public virtual void WrongKey()
    {
        Debug.Log(Name + " Is not interactable, due to the Key not being true in the savegame.");
        DialogueManager manager = gameObject.AddComponent<DialogueManager>();
        manager.dialogueFlow = FailMessage.ToArray();
        manager.IsInteractable = false;
        Sprite headSprit = Resources.Load("zache_portrait") as Sprite;
        manager.headSprite = headSprit;
        manager.StartDialogue();
    }

    // Update is called once per frame
    public virtual void Update ()
    {
        //Store if mouse was down, before releasing it.
        if(IsCurserIn)
        {
            if (Input.GetMouseButton(0))
            {
                WasCursorDown = true;
            }
            else if (Input.GetMouseButton(1))
            {
                WasCursorDown = true;
            }
            if(HoverObjectNameText && HoverNameText)
            {
                HoverObjectNameText.transform.position = Input.mousePosition.CloneWithY(Input.mousePosition.y + 20);
                HoverNameText.text = Name;
            }
        }

        //If we're having a click
        if(WasCursorDown)
        {
            if (Input.GetMouseButtonUp(0))
            {
                WasCursorDown = false;
                ActionPressed(0);
            }
            else if (Input.GetMouseButtonUp(1))
            {
                WasCursorDown = false;
                ActionPressed(1);
            }
        }
	}

    private bool isUnlocked()
    {
        if(UnlockKey.Length > 0)
        {
            bool? isUnlocked = SaveManager.GetCurrentKey(UnlockKey) as bool?;
            if (isUnlocked != true)
            {
                Debug.Log("It's locked");
                return false;
            }
        }
        return true;
    }

    private Character getCharacter()
    {
        GameObject chara = GameObject.Find("Character");
        Character cha = chara.GetComponent<Character>();
        return cha;
    }

    public bool isCharacterInReach()
    {
        if(character)
        {
            return Vector3.Distance(character.transform.position, transform.position) <= MaxInteractDistance;
        }
        Debug.LogError("Interactable could not find Character.");
        return true;
    }
}
