﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundSystem : MonoBehaviour
{
    public List<AudioClip> Sounds = new List<AudioClip>();
    public bool ShouldPlayAnySound = true;
    public float Volume = 0.1f;

    private List<AudioSource> sources = new List<AudioSource>();
    private string audioIdentifier = "#AUDIO_ISPLAYING#";

    // Use this for initialization
    void Awake ()
    {
		for(int i = 0; i < Sounds.Count; i++)
        {
            sources.Add(gameObject.AddComponent<AudioSource>());
            sources[i].clip = Sounds[i];

            bool? isActive = SaveManager.GetCurrentKey(audioIdentifier + Sounds[i].name) as bool?;
            if(isActive == true || true)
            {
                sources[i].volume = Volume;
            }
            else
            {
                sources[i].volume = 0.0f;
            }
        }
	}
	
	// Update is called once per frame
	void Update ()
    {
		if(!sources[0].isPlaying && ShouldPlayAnySound)
        {
            for (int i = 0; i < Sounds.Count; i++)
            {
                sources[i].Play();
                Debug.Log("playing");
            }
        }
	}

    public void EnableTrackByName(string trackName)
    {
        AudioClip clip = getClipByName(trackName);
        int index = Sounds.IndexOf(clip);
        fadeIn(sources[index]);
        SaveManager.SetCurrentKey(audioIdentifier + Sounds[index].name, true);
    }

    public void DisableTrackByName(string trackName)
    {
        AudioClip clip = getClipByName(trackName);
        int index = Sounds.IndexOf(clip);
        fadeOut(sources[index]);
        SaveManager.SetCurrentKey(audioIdentifier + Sounds[index].name, false);
    }

    public void AddTrack(AudioClip sound)
    {
        Sounds.Add(sound);
        AudioSource source = gameObject.AddComponent<AudioSource>();
        source.clip = sound;
        sources.Add(source);
        fadeIn(source);

        SaveManager.SetCurrentKey(audioIdentifier + sound.name, true);
    }

    private AudioClip getClipByName(string clipName)
    {
        foreach(AudioClip clip in Sounds)
        {
            if (clip.name == clipName)
                return clip;
        }
        return null;
    }
    
    private void fadeIn(AudioSource source)
    {
        StartCoroutine(AudioFade.FadeIn(source, 0.1f, Volume));
    }

    private void fadeOut(AudioSource source)
    {
        StartCoroutine(AudioFade.FadeOut(source, 0.1f));
    }
}
