﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeySetter : Interactable
{
    public enum LockType { Unlock, Lock };

    public string KeyToUnlock;
    public LockType LockOrOnlock = LockType.Unlock;
    public List<string> DialogueOnInteract = new List<string>();
    public bool CanUseMultipleTimes = false;

    private bool didUse = false;

    public override void Interact()
    {
        if((!CanUseMultipleTimes && !didUse) || CanUseMultipleTimes)
        {
            Unlock(KeyToUnlock);

            DialogueManager mgr = gameObject.AddComponent<DialogueManager>();
            mgr.dialogueFlow = DialogueOnInteract.ToArray();
            mgr.IsInteractable = false;
            Sprite headSprit = Resources.Load("zache_portrait") as Sprite;
            mgr.headSprite = headSprit;
            mgr.StartDialogue();

            didUse = true;
        }
    }

    public void Unlock(string key)
    {
        if(LockOrOnlock == LockType.Unlock)
        {
            SaveManager.SetCurrentKey(key, true);
        }
        else
        {
            SaveManager.SetCurrentKey(key, false);
        }
    }
}
