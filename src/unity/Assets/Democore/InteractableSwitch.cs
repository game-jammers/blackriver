﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InteractableSwitch : Interactable
{
    public GameObject SwitchedObject;
    public override void Interact()
    {
        bool? didDestroy = SaveManager.GetCurrentKey("DidDestroy") as bool?;
        if (didDestroy == null || didDestroy == false)
        {
            //Do the switch thingy you want here
            if (!SwitchedObject)
            {
                Debug.Log("Switched object is not set for " + Name);
                return;
            }

            Destroy(SwitchedObject);
            SaveManager.SetCurrentKey("DidDestroy", true);
        }
    }
}
