﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameJammers.BlackRiver;

public class DoorAct : Interactable {

    public string destinationScene;
    public string exitGameObjectName;

    private bool usingDoor;

    public override void Interact()
    {
        usingDoor = true;
        SceneTransition.FadeTo(destinationScene);
    }

    void OnSceneLoaded(UnityEngine.SceneManagement.Scene oldScene, UnityEngine.SceneManagement.Scene newScene)
    {
        if (usingDoor)
        {
            usingDoor = false;

            if (exitGameObjectName != "")
            {
                GameObject destDoor = GameObject.Find(exitGameObjectName);

                if (destDoor != null)
                {
                    GameObject plyChar = GameObject.Find("Character");

                    if (plyChar != null)
                        plyChar.GetComponent<Character>().transform.position = destDoor.transform.position;
                }
            }
        }
    }

    void Start()
    {
        UnityEngine.SceneManagement.SceneManager.activeSceneChanged += OnSceneLoaded;
    }
}
