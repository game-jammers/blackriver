﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveGames : MonoBehaviour {

	public void NewGame()
    {
        SaveManager.NewGame();
    }

    public void LoadGame()
    {
        SaveManager.LoadGame();
    }
}
