﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootstepSounds : MonoBehaviour {
    public AudioClip[] footsteps;
    public AudioClip[] clothing;

    private AudioSource footSource;
    private AudioSource clothSource;

    void Start()
    {
        AudioSource[] sources = GetComponents<AudioSource>();

        footSource = sources[0];
        clothSource = sources[1];
    }

    public void OnFootstep()
    {
        AudioClip randFoot = footsteps[Random.Range(0, footsteps.Length)];

        footSource.pitch = Random.Range(0.9f, 1.1f);
        footSource.clip = randFoot;
        footSource.Play();

        if (Random.Range(1, 10) == 5)
        {
            AudioClip randCloth = clothing[Random.Range(0, clothing.Length)];

            clothSource.pitch = Random.Range(0.9f, 1.1f);
            clothSource.clip = randCloth;
            clothSource.Play();
        }
    }
}
