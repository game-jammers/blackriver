﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using blacktriangles;

public static class SceneTransition
{
    // Use this instead of SceneManager.LoadScene to get the fade in and out effect.
    public static void FadeTo(string levelName)
    {
        TransitionManager manager = GameObject.FindWithTag("TransitionManager").GetComponent<TransitionManager>();

        if (manager != null)
            manager.FadeToScene(levelName);

        Cursor.SetCursor(null, Vector2.zero, CursorMode.Auto);
    }
}

public class TransitionManager : MonoBehaviour {
    public static TransitionManager TransitionInstance = null;
    public float transitionDuration;
    public GameObject fadeCanvas;

    private bool isFading = false;
    private GameObject canvasInstance;
    private UIElement fadeImage;

    // This is the main function used by the static class above.
    public void FadeToScene(string levelName)
    {
        if (!isFading)
        {
            isFading = true;

            FadeOut();
            StartCoroutine(DelayedLoad(levelName, transitionDuration));
        }
    }

    void Awake()
    {
        if (TransitionInstance == null)
            TransitionInstance = this;
        else if (TransitionInstance != this)
            Destroy(gameObject);

        SceneManager.activeSceneChanged += FadeIn; // Fade in everytime a scene is loaded.

        DontDestroyOnLoad(gameObject);
    }

    // Instantiate the fade UI, destroy it when we're done.
    void CreateFadeCanvas()
    {
        if (canvasInstance != null)
            Destroy(canvasInstance);

        canvasInstance = Instantiate(fadeCanvas, new Vector3(0, 0, 0), Quaternion.identity);
        fadeImage = canvasInstance.transform.GetChild(0).gameObject.GetComponent<UIElement>();

        // Destroy the canvas after the transition is over, so we don't get flooded with objects.
        // We times by 1.1 so there is no flash between the time that the canvas is gone, and the level is in the process of loading.
        Destroy(canvasInstance, transitionDuration * 1.1f); 
    }

    // We flip this variable off when we're done the transition.
    void fadeCallback()
    {
        isFading = false;
    }

    void FadeIn(Scene newScene, Scene oldScene)
    {
        CreateFadeCanvas();
        fadeImage.Fade(false, transitionDuration, fadeCallback);
    }

    void FadeOut()
    {
        CreateFadeCanvas();
        fadeImage.Fade(true, transitionDuration, null);
    }

    // To actually load the scene after a delay, i.e. after the fading is finished.
    IEnumerator DelayedLoad(string levelName, float time)
    {
        yield return new WaitForSeconds(time);

        SceneManager.LoadScene(levelName);
    }
}