﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameJammers.BlackRiver;

public class DialogueManager : Interactable {

    public Sprite headSprite;
    public float textSpeed = 0.04f;
    public float cancelDistance = 20f;
    public string[] dialogueFlow;
    public string[] playerLeaveLines;
    public string[] playerReturnLines;
    public string[] playerTooLongLines;
    public DialogueManager Followup;
    public AudioSource Voice;
    public Color textColor = new Color(0, 0, 0, 255);
    public float tonePitch = 1f;
    
    private int currentIndex = 0;
    private GameObject DialogueCanvas;
    private DialogueUI diagUI;
    private Image HeadTexture;

    // Use this for initialization
    public override void Awake () {
        base.Awake();
        DialogueCanvas = GameObject.FindGameObjectWithTag("DialogueCanvas");

        diagUI = DialogueCanvas.GetComponent<DialogueUI>();
        HeadTexture = DialogueCanvas.transform.GetChild(0).GetChild(1).GetComponent<Image>();
    }

    Vector3 getCharPos()
    {
        return GameObject.Find("Character").GetComponent<Character>().transform.position;
    }

    float GetPlyDistance()
    {
        return Vector3.Distance(getCharPos(), transform.position);
    }

    override public void Update()
    {
        base.Update();
        if (GameObject.Find("Character") == null)
            return;

        if (GetPlyDistance() >= cancelDistance)
        {
            Interrupt();
        }
    }

    void Interrupt()
    {

    }

    void InitUI()
    {
        currentIndex = 0;

        if (headSprite != null)
            HeadTexture.sprite = headSprite;

        UpdateText();

        diagUI.activeManager = this;
        diagUI.textSpeed = textSpeed;
        diagUI.SetTextColor(textColor);
        diagUI.tonePitch = tonePitch;
        diagUI.FadeIn();
    }

    void UpdateText()
    {
        if (GetCurrent() != "")
            diagUI.SetText(GetCurrent());
    }

    public void StartDialogue()
    {
        InitUI();
        if (Voice)
            Voice.Play();
    }

    public string GetCurrent()
    {
        if (dialogueFlow.Length - 1 >= currentIndex)
            return dialogueFlow[currentIndex];

        return "";
    }

    public void Progress()
    {
        currentIndex++;

        if (currentIndex > dialogueFlow.Length - 1)
        {
            End();
            return;
        }

        UpdateText();
    }

    public void End()
    {
        if(Followup)
        {
            Followup.StartDialogue();
        }
        else
        {
            diagUI.FadeOut();
        }
    }

    public override void Interact()
    {
        StartDialogue();
    }
}
