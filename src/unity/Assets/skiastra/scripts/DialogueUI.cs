﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using blacktriangles;
using GameJammers.BlackRiver;

public class DialogueUI : MonoBehaviour {

    public float textSpeed = 1f;
    public float fadeDuration = 0f;
    public DialogueManager activeManager;
    public static bool DidButtonClick = false;
    public float tonePitch;

    private UIElement SpeechWindow;
    private Text speechText;
    private string targetText;
    private AudioSource audioSource;

    public void SetTextColor(Color textColor)
    {
        speechText.color = textColor;
    }

    void Start()
    {
        audioSource = GetComponent<AudioSource>();
        SpeechWindow = transform.GetChild(0).GetComponent<UIElement>();
        speechText = transform.GetChild(0).GetChild(2).GetChild(0).GetComponent<Text>();

        DontDestroyOnLoad(gameObject);
    }

    public void SetText(string text)
    {
        targetText = text;
        speechText.text = "";

        StartCoroutine(UpdateText());
    }

    // To actually load the scene after a delay, i.e. after the fading is finished.
    IEnumerator UpdateText()
    {
        for (int i = 0; i <= targetText.Length; i++)
        {
            // In case the player wants to skip, so we don't keep setting the text after it gets skipped.
            if (speechText.text.Length == targetText.Length)
                break;

            speechText.text = targetText.Substring(0, i);

            audioSource.pitch = tonePitch;
            audioSource.Play();

            yield return new WaitForSeconds(textSpeed);
        }
    }

    public void OnNextButton()
    {
        Character character = GameObject.Find("Character").GetComponent<Character>();
        character.AllowedToMove = false;
        StartCoroutine(ResetClick(character));


        if (activeManager == null)
            return;

        if (speechText.text.Length < targetText.Length)
            speechText.text = targetText;
        else
            activeManager.Progress();
    }

    IEnumerator ResetClick(Character character)
    {
        yield return new WaitForSeconds(1);
        character.AllowedToMove = true;
    }

    public void FadeOut()
    {
        SpeechWindow.Fade(false, fadeDuration, null);
    }

    public void FadeIn()
    {
        SpeechWindow.Fade(true, fadeDuration, null);
    }
}
