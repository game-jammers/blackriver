﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using blacktriangles;
using System.IO;

public static class SaveManager {

    public static void PrintCurrentSave()
    {
        Debug.Log(GetCurrentSave().ToJson());
    }

    // Set a value in the currently loaded save file, this will get saved in the save file when the player saves their game.
    public static void SetCurrentKey(string k, object v)
    {
        currentSave.SetKey(k, v);
        SaveGame();

    //    Debug.Log(k + ": " + GetCurrentKey(k));
    }

    // Get a value from the currently loaded save file, this is how you will check if certain events have occurred in the currently loaded save.
    public static object GetCurrentKey(string k)
    {
        return currentSave.GetKey(k);
    }

    // Create a new save file to be used with the Get and Set Key functions, use this in a start new game feature.
    public static void NewGame(string saveName = "savegame")
    {
     //   Debug.Log("New Game");

        currentSave = new SaveTable(saveName);
        SaveGame();
    }

    // Load a save file to be used with the Get and Set Key functions, use this in a load game feature.
    public static void LoadGame(string saveName = "savegame")
    {
        string filePath = GetSavePath() + saveName + ".sav";

        if (File.Exists(filePath))
        {
            string saveData = File.ReadAllText(filePath);

            if (!saveData.Equals(""))
                currentSave = new SaveTable(saveName, saveData);

                Debug.Log("Loading Game '" + saveName + "'. Printing contents.");
                PrintCurrentSave();
        }
    }

    // Save the currently loaded save table, use this in a save game feature.
    public static void SaveGame()
    {
        // Debug.Log("Saving Game!");
        Directory.CreateDirectory(GetSavePath());

        string path = currentSave.GetPath();

        if (File.Exists(path))
            File.Delete(path);

        File.WriteAllText(path, currentSave.ToJson());
    }

    // The class for the save objects containing all the keys.
    [System.Serializable]
    public class SaveTable : Dictionary<string, object>
    {
        public string name;

        public SaveTable(string fileName, string jsonData = "")
        {
            name = fileName;

            if (!jsonData.Equals(""))
            {
                JsonObject json = JsonObject.FromString(jsonData);

                foreach (string k in json.keys)
                {
                    this[k] = json[k];
                }
            }
        }

        // Get a value within the list based on the identifer supplied.
        public object GetKey(string key)
        {
            if (ContainsKey(key))
                return this[key];

            return null;
        }

        // Set a value within the list based on the identifier and value supplied.
        public void SetKey(string key, object value)
        {
            this[key] = value;
        }

        // Convert the save list of into a json string.
        public string ToJson()
        {
            JsonObject json = new JsonObject();

            foreach (KeyValuePair<string, object> pair in this)
            {
                json[pair.Key] = pair.Value;
            }

            return json.ToString();
        }

        public string GetPath()
        {
            return GetSavePath() + name + ".sav";
        }
    }

    // The currently selected save file, the one the player is currently loaded into via the NewGame or LoadGame methods.
    private static SaveTable currentSave = new SaveTable("testing");

    // Get the list of save file paths, used for loading and saving.
    public static string[] GetSaves()
    {
        return Directory.GetFiles(GetSavePath(), "*.sav");
    }

    // Returns the save path that all saves will save in.
    private static string GetSavePath()
    {
        return Application.dataPath + "/saves/";
    }

    // Get the currently loaded SaveTable object.
    public static SaveTable GetCurrentSave()
    {
        return currentSave;
    }
}
