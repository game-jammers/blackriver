//
// GameJammers 2017
//

using blacktriangles;
using System.Collections.Generic;
using UnityEditor;
using UnityEditorInternal;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    [CustomEditor(typeof(Dialogue))]
    public class DialogInspector
        : Editor
    {
        // members /////////////////////////////////////////////////////////////
        public List<string> items                           = new List<string>();

        private ReorderableList reorderableList             = null;

        private SerializedProperty spriteProp               = null;
        private SerializedProperty flowProp                 = null;
        private SerializedProperty nameProp                 = null;

        // unity callbacks /////////////////////////////////////////////////////
        public override void OnInspectorGUI()
        {
            items = new List<string>();
            items.Add( "[NONE]" );
            items.AddRange( Database.stringtable.GetDialogIds() );

            spriteProp = serializedObject.FindProperty( "HeadSprite"  );
            flowProp = serializedObject.FindProperty( "dialogueFlow" );
            nameProp = serializedObject.FindProperty( "Name" );
            serializedObject.Update();

            SerializedProperty prop = serializedObject.GetIterator();
            prop.Next( true );

            while( prop.NextVisible( false ) )
            {
                if( prop.name == "DialogId" )
                {
                    GUILayout.BeginVertical( GUI.skin.box );
                        EditorGUI.BeginChangeCheck();
                        int id = items.IndexOf( prop.stringValue );
                        id = EditorGUILayout.Popup( id, items.ToArray() );
                        if( items.IsValidIndex( id ) && id > 0 )
                        {
                            prop.stringValue = items[id];
                        }
                        else
                        {
                            prop.stringValue = System.String.Empty;
                        }

                        if( EditorGUI.EndChangeCheck() )
                        {
                            StringTable.DialogEntry entry = Database.stringtable.GetDialog( prop.stringValue );
                            if( entry != null )
                            {
                                string[] flow = entry.dialog.Replace( "\\n", "\n" ).Split( '\n' );
                                flowProp.ClearArray();
                                foreach( string flowItem in flow )
                                {
                                    flowProp.InsertArrayElementAtIndex( flowProp.arraySize );
                                    flowProp.GetArrayElementAtIndex( flowProp.arraySize - 1 ).stringValue = flowItem;
                                }

                                StringTable.CharacterEntry character = Database.stringtable.GetCharacter( entry.speaker );
                                if( character != null )
                                {
                                    nameProp.stringValue = System.String.Format( "<color=\"{0}\">{1}</color>", character.color.ToHexString(), character.displayName );
                                    spriteProp.objectReferenceValue = character.portrait;
                                }
                            }
                        }
                    GUILayout.EndVertical();
                }
                else if( prop.name == "dialogueFlow" )
                {
                }
                else
                {
                    EditorGUILayout.PropertyField( prop );
                }
            }

            if( reorderableList == null )
            {
                bool draggable = true,
                    displayHeader = true,
                    displayAddHeader = true,
                    displayRemoveButton = true;

                reorderableList = new ReorderableList( flowProp.serializedObject
                                          , flowProp
                                          , draggable
                                          , displayHeader
                                          , displayAddHeader
                                          , displayRemoveButton
                    );

                reorderableList.drawHeaderCallback = ( position )=>{ GUI.Label( position, "Dialog Flow" ); };
                reorderableList.drawElementCallback = ( position, index, isActive, isFocused )=>{
                        SerializedProperty entryField = reorderableList.serializedProperty.GetArrayElementAtIndex( index );
                        entryField.stringValue = EditorGUI.TextField( position, entryField.stringValue );
                    };
            }

            reorderableList.DoLayoutList();

            serializedObject.ApplyModifiedProperties();
        }
    }
}
