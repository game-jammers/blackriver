//=============================================================================
//
// (C) BLACKTRIANGLES 2015
// http://www.blacktriangles.com
// contact@blacktriangles.com
//
// Howard N Smith | hsmith | howard@blacktriangles.com
//
//=============================================================================

using UnityEngine;
using UnityEditor;
using System;
using System.IO;

namespace GameJammers.BlackRiver
{
    public static class DebugMenu
    {
		// public methods /////////////////////////////////////////////////////
        [MenuItem("Tools/BlackRiver/Rebuild Database")]
        static void RebuildDatabase()
        {
            Database.Initialize( true );
        }
	}
}
