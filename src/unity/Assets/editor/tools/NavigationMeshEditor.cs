//
// Gmae Jammers 2017
//

using blacktriangles;
using UnityEditor;
using UnityEngine;

namespace GameJammers.BlackRiver
{
    [CustomEditor(typeof(NavigationMesh))]
    public class NavigationMeshEditor
        : Editor
    {
        // members /////////////////////////////////////////////////////////////
        private bool isDragging                             = false;
        private Vector3 startDrag                           = Vector3.zero;
        SerializedProperty areasProp                        = null;

        private int selectedIndex                           = -1;
        private Vector3 mousePosition                       = Vector3.zero;

        // unity callbacks /////////////////////////////////////////////////////
        private void OnEnable()
        {
            isDragging = false;
            areasProp = serializedObject.FindProperty( "areas" );
        }

        public override void OnInspectorGUI()
        {
            serializedObject.Update();
            EditorGUILayout.HelpBox( "Press and Drag Right Mouse Button to create an area.  Press middle mouse button to select an area.  Press \"x\" key to delete an area.", MessageType.Info );
            if( GUILayout.Button( "Clear All" ) )
            {
                areasProp.ClearArray();
            }

            if( selectedIndex >= 0 && areasProp != null && selectedIndex < areasProp.arraySize )
            {
                SerializedProperty selbox = areasProp.GetArrayElementAtIndex( selectedIndex );
                if( selbox != null )
                {
                    EditorGUILayout.PropertyField( selbox );
                }
            }

            for( int i = 0; i < areasProp.arraySize; ++i )
            {
                bool isChecked = ( i == selectedIndex );
                isChecked = EditorGUILayout.Toggle( i.ToString(), isChecked );
                if( isChecked )
                {
                    selectedIndex = i;
                    Repaint();
                }
            }

            if( GUILayout.Button( "Build Nav Grid" ) && target != null )
            {
                NavigationMesh navmesh = target as NavigationMesh;
                if( navmesh != null && SceneView.currentDrawingSceneView != null )
                {
                    navmesh.RebuildGraph( SceneView.currentDrawingSceneView.camera );
                }
            }

            serializedObject.ApplyModifiedProperties();
        }

        private void OnSceneGUI()
        {
            serializedObject.Update();


            if( Event.current.isMouse )
            {
                mousePosition = Event.current.mousePosition;
                Camera sceneCamera = SceneView.currentDrawingSceneView.camera;
                mousePosition.y = sceneCamera.pixelHeight - mousePosition.y;
                mousePosition = btCamera.ScreenToWorld( sceneCamera, mousePosition, new Plane( Vector3.forward, Vector3.zero ) );

                if( Event.current.button == 1 && Event.current.type == EventType.MouseDown )
                {
                    isDragging = true;
                    startDrag = mousePosition;
                    Event.current.Use();
                }
                else if( Event.current.button == 1 && Event.current.type == EventType.MouseUp )
                {
                    isDragging = false;
                    Rect rect = new Rect();
                    rect.min = new Vector2( Mathf.Min( startDrag.x, mousePosition.x ), Mathf.Min( startDrag.y, mousePosition.y ) );
                    rect.max = new Vector2( Mathf.Max( startDrag.x, mousePosition.x ), Mathf.Max( startDrag.y, mousePosition.y ) );
                    areasProp.InsertArrayElementAtIndex( areasProp.arraySize );
                    areasProp.GetArrayElementAtIndex( areasProp.arraySize-1 ).rectValue = rect;
                    Event.current.Use();
                }
                else if( Event.current.button == 2 && Event.current.type == EventType.MouseUp )
                {
                    for( int i = 0; i < areasProp.arraySize; ++i )
                    {
                        Rect rect = areasProp.GetArrayElementAtIndex( i ).rectValue;
                        if( rect.Contains( mousePosition ) )
                        {
                            selectedIndex = i;
                        }
                    }
                    Event.current.Use();
                }
            }
            else if( Event.current.isKey && Event.current.keyCode == KeyCode.X && Event.current.type == EventType.KeyUp )
            {
                areasProp.DeleteArrayElementAtIndex( selectedIndex );
                Event.current.Use();
            }

            for( int i = 0; i < areasProp.arraySize; ++i )
            {
                Color color = ( i == selectedIndex ) ? new Color( 0.5f, 0.5f, 0.0f, 0.5f ) : new Color(0.0f, 0.5f, 0.0f, 0.5f);

                Rect rect = areasProp.GetArrayElementAtIndex( i ).rectValue;
                Handles.DrawSolidRectangleWithOutline( rect.ToVertices(), color, color );
            }

            if( isDragging )
            {
                Rect rect = new Rect();
                rect.min = new Vector2( Mathf.Min( startDrag.x, mousePosition.x ), Mathf.Min( startDrag.y, mousePosition.y ) );
                rect.max = new Vector2( Mathf.Max( startDrag.x, mousePosition.x ), Mathf.Max( startDrag.y, mousePosition.y ) );
                Handles.DrawSolidRectangleWithOutline( rect.ToVertices(), new Color(0.0f, 0.5f, 0.0f, 0.5f), new Color( 0.0f, 0.0f, 0.0f, 1.0f ) );
            }

            if( Event.current.type == EventType.Repaint )
            {
                NavigationMesh navmesh = target as NavigationMesh;
                if( navmesh != null && navmesh.navNodes != null )
                {
                    const float step = NavigationMesh.kStep;
                    for( int y = 0; y < navmesh.dims.y; ++y )
                    {
                        //Handles.DrawLine( new Vector3( 0f, y * step, 0f ), new Vector3( navmesh.dims.x * step, y * step, 0f ) );
                        for( int x = 0; x < navmesh.dims.x; ++x )
                        {
                            //Handles.DrawLine( new Vector3( x * step, 0, 0f ), new Vector3( x * step, navmesh.dims.y * step, 0f ) );
                            NavigationMesh.NavNode node = navmesh.GetNavNode( x, y );
                            if( node != null)
                            {
                                //Handles.CircleCap( 0, node.worldPos, Quaternion.identity, step * 0.25f );
                                foreach( NavigationMesh.NavNode neighbor in node.connections )
                                {
                                    Handles.DrawLine( node.worldPos, neighbor.worldPos );
                                }
                            }
                        }
                    }
                }
            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}
